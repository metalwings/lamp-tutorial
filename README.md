# Webserver erstellen
Von [Georgi Yanev](https://goshko.de), BIF32a

Wenn eine dritte Person von außen auf unseren Server zugreifen möchte, mus es eine Art Software geben, die diese Anfragen delegiert. Dafür bietet sich **Apache** an.

Für unseren Webserver werden wir den *LAMP*-Stack verwenden.
**L**inux als Betriebsystem, wodrunter
**A**pache die Anfragen delegiert und
**M**ariaDB Daten speichert, die später von
**P**HP für die Anzeige der Webseite verwendet werden.

### Apache installieren
```bash
$ sudo apt update # um unsere Repositories zu aktualisieren
$ sudo apt install apache2 # das Apache2 Paket installieren
```

Sobald die Installation erfolgreich ist, kann man den Server schon unter `http://172.22.113.115` aufrufen.

![Default Apache Page][apache-default-page]

### MariaDB installieren
```bash
$ sudo apt install mariadb-server # Skript für die MariaDB Installation herunterladen
$ sudo mysql_secure_installation # Skript starten
```
Nun folgende Fragen beantworten:
```bash
Enter current password for root(enter for none): # Enter drücken
Change the root password? [Y/n] # y drücken und Passwort setzen
Remove anonymous users? [Y/n] # y drücken, um alle andere user zu entfernen
Disallow root login remotely? [Y/n] # wir brauchen kein Zugriff von außen, also y
Remove test database and access to it? [Y/n] # y
Reload privilege tables now? [Y/n] # y
```

Soweit sollte die MariaDB Installation erfolgreich sein.
```bash
# so können wir auf die Datenbank zugreifen
$ sudo mysql
```

### PHP 

#### Installation
```bash
$ sudo apt install php7.0 libapache2-mod-php7.0 php7.0-mysql
# php7 ist das offizielle Paket von PHP
# libapache2-mod-php benötigt man für die Anbindung zwischen apache und php
# php-mysql benötigt man für die Anbindung zwischen php und mysql
```

#### Konfiguration

Das default-Verhalten von Apache ist, immer eine `index.html` zu suchen und diese anzuzeigen. Da wir nun PHP nutzen wollen, müssen wir die `index.php` priorisieren, indem wir die `/etc/apache2/mods-enabled/dir.conf` wie folgt modifizieren:

```bash
$ sudo vim /etc/apache2/mods-enabled/dir.conf

<IfModule mod_dir.c>
  DirectoryIndex index.php index.html index.cgi index.pl index.xhtml index.htm
</IfModule>
```

Damit die Anpassungen in Kraft treten, müssen wir den apache Service neustarten
```bash
$ sudo systemctl restart apache2
```

### Beispielseite mit PHP

Um alles zu testen erstellen wir nun eine Gästebuch Seite. 

```bash
$ cd /var/www/html # hier liegen unsere Dateien 
$ sudo rm index.html
$ sudo mkdir guestbook && cd guestbook

# hier laden wir den Inhalt von guestbook/ hoch
```

### Datenbank einrichten
Für unseren Gästebuch brauchen wir eine Datenbank, wo wir die Einträge speichern werden.
Um diese zu erstellen importieren wir den `comments.sql` Skript

```bash
# ausgenommen wir befinden uns im root dieser Repository
$ mysql -uroot -p # in mysql einloggen 
  > source comments.sql # Skript ausführen
```

**Apache2 neustarten, damit alle Änderungen in Kraft treten**

```bash
$ sudo systemctl restart apache2
```

Seite unter `http://172.22.113.115/lamp-tutorial/guestbook/` aufrufbar

[apache-default-page]: images/apache-default-page.PNG