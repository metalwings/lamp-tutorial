CREATE DATABASE gy_webserver_example;
USE gy_webserver_example;

CREATE TABLE comments ( 
  `comment_id` INT AUTO_INCREMENT,
  `from` VARCHAR(255),
  `date` DATE,
  `message` TEXT,
  PRIMARY KEY (comment_id)
) ENGINE=INNODB;