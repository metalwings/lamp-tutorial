<?php 
  require("functions.php");
  ini_set('display_errors', 1);
  ini_set('display_startup_errors', 1);
  error_reporting(E_ALL);
?>

<!DOCTYPE HTML>
<html lang="de" dir="ltr">

<head>
  <meta charset="utf-8" />
  <title>Georgi's Gästebuch</title>
  <link rel="stylesheet" href="./resources/materialize.min.css">
</head>

<body>
  <nav>
    <div class="nav-wrapper">
      <a href="#" class="brand-logo center">Georgi's Gästebuch</a>
    </div>
  </nav>

  <div class="row" style="padding-top: 60px;">
    <?php 
      $comments = get_all_comments();
      foreach ($comments as $comment) {
        echo "<p>" . $comment['message'] . " - " . ($comment['from'] == null ? 'anon' : $comment['from']) . "</p>";
      }
    ?>
  </div>

  <script src="./resources/materialize.min.js"></script>

</body>

</html>